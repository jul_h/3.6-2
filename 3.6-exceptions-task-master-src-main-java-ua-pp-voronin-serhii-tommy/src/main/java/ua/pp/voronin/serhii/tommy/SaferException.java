package ua.pp.voronin.serhii.tommy;

// Змінити клас так, щоб його можна було використовувати у якості винятку
public class SaferException extends RuntimeException {
    public SaferException (String message) {
      super(message);
    }
}
